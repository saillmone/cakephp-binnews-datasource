# Binnews (binnews.in) Datasource pour CakePHP

Plugin CakePHP pour accéder aux flux RSS et à la recherche de Binnews via une source de données.

### Objectif de ce plugin
L'objectif de ce plugin est de fournir un accés simple aux flux RSS de Binnews et à la recherche de Binnews. Les résultats ("releases") retournés peuvent être enrichis grâce aux informations provenant de l'API de The Movie Database.

Catégories supportées : HD, SD and DVD Films & Anime

### Est-ce que ça marche ?

Plutôt pas mal : http://www.movizdata.tk

### Pré-requis

- CakePHP version 2.0
- [Optionnel] Plugin CakePHP-TMDB-Datasource (https://github.com/angelxmoreno/CakePHP-TMDB-Datasource)

## Installation et configuration

        git@bitbucket.org:saillmone/cakephp-binnews-datasource.git
        ou
        https://saillmone@bitbucket.org/saillmone/cakephp-binnews-datasource.git
        
Récupérez le code selon votre méthode habituelle (Composer, Submodule, Clone, etc.) et placez-le dans le répertoire app/Plugin/BinnewsApi.

### Activation du plugin

Activez le plugin dans le fichier app/Config/bootstrap.php.

    CakePlugin::load('BinnewsApi');
    
Si vous utilisez déjà `CakePlugin::loadAll();`, ce n'est pas nécessaire.

### Configuration de la base de donnée

Dans app/Config/database.php, ajoutez la configuration suivante :

    public $binnews = array(
        'datasource' => 'BinnewsApi.Binnews',
        'database' => false,
    );

### Configuration d'un *model*

Liez la base de données à vos *models* selon vos besoins comme ceci par exemple :

    // app/Models/BinnewsMovie.php
    class BinnewsMovie extends AppModel {
        public $useDbConfig = 'binnews';
    }
    
### Contenu enrichi par The Movie Database API

Si vous souhaitez que les releases soient enrichies avec les informations provenant de The Movie Database, vous devez configurer le plugin CakePHP-TMDB-Datasource (https://github.com/angelxmoreno/CakePHP-TMDB-Datasource) tel que défini dans son Readme.md.

Le plugin CakePHP-Binnews-Datasource appelle le *model* lié à base de données du plugin CakePHP-TMDB-Datasource de la façon suivante :

    $TmdbMovie = ClassRegistry::init('TmdbMovie');
    
Ce *model* sus-cité doit donc **impérativement** s'appeler `app/Models/TmdbMovie.php`.

## Utilisation

D'une manière générale, requêtez la base de données de la même manière que vous le feriez pour une base de données "classique", à la sauce CakePHP (c'est le but !).

### Lire les flux RSS

Par défaut, si le paramètre *search* n'est pas précisé, le plugin lit les flux RSS.

Exemple  :

    //  app/Controllers/BinnewsMoviesController.php
    $movies = $this->BinnewsMovie->find('all');
    
Le code ci-dessus va lire tous les flux RSS.
 
### Faire une recherche
    
Il suffit d'ajouter le paramètre *search*.

Exemple :

    //  app/Controllers/BinnewsMoviesController.php
    $movies = $this->BinnewsMovie->find('all', 'search' => 'Matrix' );
    
### Enrichir les résultats avec The Movie Database

Vous pouvez enrichir les résultats, quelque soit la requête (flux RSS ou recherche). Il suffit d'ajouter le paramètre *fields* et la valeur *TmdbApi*.

Exemple:

    //  app/Controllers/BinnewsMoviesController.php
    $movies = $this->BinnewsMovie->find('all', array(
        'fields' => 'TmdbApi',
        'search' => 'Matrix'
    );
    
## Paramètres

### Communs (flux RSS et recherche)
    
    array(
        'fields' => '' // '__count' ou 'TmdbApi',
        'cat' => array( // array ou string
            'sdmovie',
            'hdmovie',
            'dvdmovie',
            'sdanime',
            'hdanime',
            'dvdanime'
        ),
        'order' => array(
            'field' => '' // nom du champ + asc ou desc
        ),
        'limit' => '',
        'page' => ''
    );

### Seulement sur les flux RSS

    array(
        'conditions' => array('field' => 'condition') // cherche 'condition' dans 'field'
    );

### Seulement sur la recherche

    array(
        'search' => null, // string
        'conditions' => array(
        	'chkFichier' => 'on', // recherche sur les noms de fichiers
		    'chkInit' => 1,
		    'chkTitre' => 'on', // recherche sur les titres
		    'chkCat' => 'on', // recherche sur les catégories
		    'edAge' => 1900, // âge maximum des releases
		    'lg' => array() // 'all' ou 'FR', 'VO', 'VOST'),
		    'resHD' => array() // 'all' ou '720', '1080', 'SD')
        )
    );