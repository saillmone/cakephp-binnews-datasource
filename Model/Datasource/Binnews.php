<?php
/**
 * Binnews Datasource - A CakePHP datasource for Binnews (http://www.binnews.in)
 *
 * API Documentation:
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt file
 * Redistributions of files must retain the above copyright notice.
 *
 * @author        Simon HOUSSIN (aka saillmone)
 * @link
 * @package    datasources
 * @license        http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Xml', 'Utility');
App::uses('HttpSocket', 'Network/Http');

class Binnews extends DataSource
{
    /**
     * An optional description of the datasource
     */
    public $description = 'A CakePHP datasource for Binnews (http://www.binnews.in)';

    public $helpers = array('Movie');

    /**
     * Default configuration options
     *
     * @var array
     * @access public
     */
    public $_baseConfig = array(
        'encoding' => 'UTF-8',
        'cacheTime' => '+1 hour',
        'cacheType' => 'movies',
        'version' => '2.0'
    );

    /**
     * Default search conditions
     *
     * @var array
     * @access public
     */
    public $_postfields = array(
        'chkFichier' => 'on',
        'chkInit' => 1,
        'chkTitre' => 'on',
        'chkCat' => 'on',
        'edAge' => 1900,
        'edTitre' => null,
        'cats' => null,
        'lg' => array('all'),
        'resHD' => array('all')
    );

    /**
     * Binnews categories authorized (compatibles)
     *
     * @var array
     * @access public
     */
    private $_cat = array(
        'sdmovie' => 6,
        'hdmovie' => 39,
        'dvdmovie' => 23,
        'sdanime' => 27,
        'hdanime' => 49,
        'dvdanime' => 48,
        'sdshow' => 36,
        'hdshow' => 36,
        'dvdshow' => 36
    );

    /**
     * name function.
     *
     * @access public
     * @param mixed $name
     * @return void
     */
    public function name($name)
    {
        return $name;
    }

    /**
     * read function.
     *
     * @access public
     * @param object Model $model
     * @param array $queryData
     * @return array
     */
    public function read(Model $model, $queryData = array(), $recursive = null)
    {
        $this->log("Début d'exécution du plugin Binnews", 'debug');

        // necessary for Tmdb search
        set_time_limit(300);

        // Cache duration
        Cache::set(
            array('duration' => $this->config['cacheTime']),
            NULL,
            $this->config['cacheType']
        );

        // are categories defined ?
        if (Set::extract($queryData, 'cat')) {
            if (is_string($queryData['cat'])) {
                if (array_key_exists($queryData['cat'], $this->_cat))
                    $cats[$queryData['cat']] = $this->_cat[$queryData['cat']];
            } else {
                foreach ($queryData['cat'] as $cat) {
                    if (array_key_exists($cat, $this->_cat))
                        $cats[$cat] = $this->_cat[$cat];
                }
            }
        } else
            $cats = $this->_cat;

        $cats = array_unique($cats);

        $releases = array();

        // search or list feeds ?
        if ($search = Set::extract($queryData, 'search')) {

            $this->log('Type de la requête : recherche', 'debug');

            $data = $this->__readHtmlData($search, $cats, $queryData['conditions']); // include filters in search

            if (!$data) return false;

            $releases = $this->__parseHtmlItems($data);

            $releases = $this->__mergeReleases($releases);

            // query the TMDb database for complete movie information
            // IMPORTANT !!! Requires the CakePHP-TMDB-Datasource plugin !
            // https://github.com/angelxmoreno/CakePHP-TMDB-Datasource
            if ($queryData['fields'] === 'TmdbApi') {
                $releases = $this->__tmdbFind($releases);
            }
        } else {
            $this->log('Type de la requête : flux RSS', 'debug');
            foreach ($cats as $cat => $catId) {
                $cacheName = 'binnews_' . $cat;
                $this->log('Lecture du cache de la catégorie '. $cat, 'debug');
                $items = Cache::read($cacheName, $this->config['cacheType']);

                if ($items === false) {
                    $this->log('Le cache n\'est plus valide : génération du cache', 'debug');
                    $this->log('Récupération du flux RSS', 'debug');
                    $data = $this->__readXmlData($catId);
                    $this->log('Parsage du flux RSS', 'debug');
                    $items = $this->__parseXmlItems($data, $cat);

                    // merge binnews releases
                    $this->log('Concaténation des '. count($items) .' releases', 'debug');
                    $items = $this->__mergeReleases($items);
                    $this->log('Résultat de la concaténation : '. count($items), 'debug');

                    // query the TMDb database for complete movie information
                    // IMPORTANT !!! Requires the CakePHP-TMDB-Datasource plugin !
                    // https://github.com/angelxmoreno/CakePHP-TMDB-Datasource
                    if ($queryData['fields'] === 'TmdbApi') {
                        $this->log('Complétion des releases avec les infos Tmdb', 'debug');
                        $items = $this->__tmdbFind($items);
                    }

                    // write cache
                    $this->log('Ecriture du cache', 'debug');
                    Cache::write($cacheName, $items, $this->config['cacheType']);
                }
                else
                    $this->log('Le cache est encore valide', 'debug');

                $this->log('Application des conditions si elles existent', 'debug');
                $items = $this->__filterItems($items, $queryData['conditions']);
                $releases = array_merge($items, $releases);
            }
        }

        // merge movies
        $this->log('Concaténation des '. count($releases) .' films', 'debug');
        $releases = $this->__mergeMovies($releases);
        $this->log('Résultat de la concaténation : '. count($releases), 'debug');

        if (!empty($releases)) {
            // orders
            $this->log('Ordonne', 'debug');
            $releases = $this->__sortItems($model, $releases, $queryData['order']);

            // pagine
            $this->log('Pagine', 'debug');
            $releases = $this->__getPage($releases, $queryData);

            if (Set::extract($queryData, 'fields') == '__count') {
                return array($model->alias => array('count' => count($releases)));
            }
        } else {
            if (Set::extract($queryData, 'fields') == '__count') {
                return array($model->alias => array('count' => count($releases)));
            }
        }

        // define model->alias
        foreach ($releases as $release) {
            $_releases[] = array($model->alias => $release);
        }

        $this->log('Fin de la requête', 'debug');
//        debug($_releases); exit;
        return $_releases;
    }

    /**
     * __readHtmlData function.
     *
     * @access public
     * param string $cats
     * @return void
     */
    public function __readHtmlData($search, $cats = null, $conditions = null)
    {

        if (!empty($conditions)) {
            foreach ($conditions as $field => &$condition) {
                $filter = $this->__passesHtmlCondition($field, $condition);
                if ($filter)
                    $condition = $filter;
            }
            $postfields = array_merge($this->_postfields, $conditions);
        } else
            $postfields = $this->_postfields;

        if (!empty($cats))
            $postfields['cats'] = $cats;

        if (!empty($search))
            $postfields['edTitre'] = htmlspecialchars($search);

        $HttpSocket = new HttpSocket();

        $response = $HttpSocket->post(
            'http://www.binnews.in/_bin/search2.php',
            $postfields
        );

        @preg_match("|<table id=\"tabliste\".*>(.*)</table>|", $response, $tbody);

        if (empty($tbody[0])) {
            return false;
        }

        return $tbody[0];
    }

    /**
     * __passesHtmlCondition function.
     *
     * @access public
     * @param mixed $field
     * @param mixed $condition
     * @return void
     */
    public function __passesHtmlCondition($field, $condition)
    {
        if (!in_array($field, $this->_postfields))
            return false;

        switch ($field) {
            case 'lg':
                if ($condition == 'FR')
                    $newCondition = array(2, 5, 6, 8, 9, 10);
                elseif ($condition == 'VO')
                    $newCondition = array(5, 6, 10, 17);
                elseif ($condition == 'VOST')
                    $newCondition = array(7);
                elseif ($condition == 'all')
                    $newCondition = array('all');
                else
                    throw new Exception('Valeur de la langue incorrecte');
                break;

            case 'resHD':
                if ($condition == '720')
                    $newCondition = array(2);
                elseif ($condition == '1080')
                    $newCondition = array(3, 4);
                elseif ($condition == 'SD')
                    $newCondition = array(1);
                elseif ($condition == 'all')
                    $newCondition = array('all');
                else
                    throw new Exception('Valeur de la résolution incorrecte');
                break;

            case 'edAge':
                if (!is_numeric($condition))
                    throw new Exception('Valeur de l\'âge non numérique');
                else
                    $newCondition = $condition;
                break;
        }

        return $newCondition;
    }

    /**
     * __parseHtmlItems function.
     *
     * @access public
     * @param mixed $items
     * @return void
     */
    public function __parseHtmlItems($data)
    {
        $releases = array();

        libxml_use_internal_errors(true);

        $dom = new domDocument;
        $dom->loadHTML(utf8_decode($data)); // important!
//    $dom->loadHTML($data);
        $dom->preserveWhiteSpace = false;

        $finder = new DomXPath($dom);

        $items = $finder->query("//tr[contains(@class, 'ligne')]");

        foreach ($items as $item) {

            $release = array();

            $node = $finder->evaluate("string(.//td[3]/a[@class='c16'])", $item);

            if (stripos($node, 'A Tous Ceux Qui Prennent Des Films')
                OR stripos($node, 'Un Fichier Avec Mdp ? Lisez Ceci')
                OR stripos($node, '*** MOT DE PASSE ***')
                OR stripos($node, '*** FAKE ***')
                OR stripos($this->__low($node), 'a supprimer')
                OR empty($node)
            )
                continue;

            preg_match_all('#([^(]*)(\(.*\)?)?\(([0-9]{4})?\)?#',
                $node,
                $res,
                PREG_SET_ORDER);

            if (empty($res)) continue;

            if (stripos($res[0][1], '3d'))
                $release['b_3d'] = true;

            $title = str_ireplace(array(
                '-', '3d', '(', ')', '[', ']', 'disque 1', 'disque 2', 'version longue', 'BD Européen'
            ), '', $res[0][1]);

            $trans = array('.' => ' ');
            $title = trim(strtr($title, $trans));

            $release['b_title'] = $title;

            $release['b_year'] = !empty($res[0][3]) ? trim(intval($res[0][3])) : ''; // date

            $release['b_resolution'] = trim($finder->evaluate("string(.//td[3]/span[@class='c3'])", $item), "{, }");

            $release['b_lang'] = $finder->evaluate("string(.//td[4])", $item);
            $release['b_lang'] .= $finder->evaluate("string(.//td[4]/img/@alt)", $item);

            $node = $finder->query(".//td[5]/a[@class='c16']", $item);
            foreach ($node as $n) {
                $release['b_group'][] = $n->nodeValue;
            }

            $release['b_name'] = $finder->evaluate("string(.//td[6])", $item);

            $release['b_size'] = rtrim($finder->evaluate("string(.//td[7])", $item), " +");

            $release['b_nfo'] = "http://www.binnews.info" . $finder->evaluate("string(.//td[9]/a/@href)", $item);

            $release['b_comments'] = $finder->evaluate("string(.//td[10]/a/@href)", $item);

            $release['b_spoter'] = $finder->evaluate("string(.//td[11])", $item);

            $releases[] = $release;
        }

        return $releases;
    }

    /**
     * Enlève les caractères spéciaux, formatte en minuscule
     *
     * @access public
     * @param string $str
     * @return void
     */
    public function __low($str)
    {
        $str = strtolower(strtr(
            $str,
            "ÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ",
            "aaaaaaaaaaaaooooooooooooeeeeeeeecciiiiiiiiuuuuuuuuynn"
        ));

        return trim($str);
    }

    /**
     * Merge Binnews releases
     *
     * @access public
     * @param mixed $releases
     * @return void
     */
    public function __mergeReleases($releases)
    {
        $releases = Set::sort($releases, '{n}.b_title', 'asc');
        $releases_merge = array();

        foreach ($releases as $r) {
            $key = $this->array_search_r($r['b_title'], $releases_merge);
            if ($key === false) {
                $releases_merge[] = array(
                    'b_title' => $r['b_title'],
                    'b_year' => $r['b_year'],
                    'releases' => array(array_splice($r, 2))
                );
            } else {
                $releases_merge[$key]['releases'][] = array_splice($r, 2);
            }
        }

        return $releases_merge;
    }

    /**
     * array_search recursive
     *
     * @access private
     * @return void
     */
    private function array_search_r($needle, $haystack)
    {
        foreach ($haystack as $key => $value) {
            $current_key = $key;
            if ($needle === $value OR (is_array($value) && self::array_search_r($needle, $value) !== false)) {
                return $current_key;
            }
        }
        return false;
    }

    /**
     * __tmdbFind function.
     *
     * IMPORTANT !!! Requires the CakePHP-TMDB-Datasource plugin !
     * https://github.com/angelxmoreno/CakePHP-TMDB-Datasource
     *
     * @access public
     * @param mixed $releases
     * @return void
     */
    public function __tmdbFind($releases)
    {
        $TmdbMovie = ClassRegistry::init('TmdbMovie');

        $movies = array();

        $i = 0; // max : 40 queries/10 sec on Tmdb
        foreach ($releases as $release) {
            if (($i + 1) % 5 == 0)
                sleep(1); // sleep for 1 sec/5 queries

            $tmdbMovies = $TmdbMovie->find('all', array('conditions' => array(
                'query' => $release['b_title'],
                'append_to_response' => true
            )));

            if (!empty($tmdbMovies)) {
                $movie = $this->__choose($release, $tmdbMovies);

                $movies[] = array_merge($release, $movie['TmdbMovie']);
            } else {
                $movies[] = $release;
            }
            $i++;
        }

        $movies = $this->__mergeMovies($movies);

        return $movies;
    }

    /**
     * Selects the Tmdb movie more approaching the Binnews release
     *
     * @access public
     * @param mixed $release
     * @param mixed $results
     * @return void
     */
    public function __choose($release, $results)
    {

        $nbr = count($results);

        if ($nbr < 1)
            return $release;

        $b_title = $this->__low($release['b_title']);
        $b_year = $release['b_year'];

        for ($i = 0; $i < $nbr; $i++) {
            $a_title = $this->__low($results[$i]['TmdbMovie']['title']);
            $a_oTitle = $this->__low($results[$i]['TmdbMovie']['original_title']);
            $a_year = date('Y', strtotime($results[$i]['TmdbMovie']['release_date']));

            $year = array(
                90 => $b_year,
                80 => $b_year + 1,
                70 => $b_year - 1,
                60 => $b_year + 2,
                50 => $b_year - 2
            );

            if ($lucky = array_search($a_year, $year)) {
                if (($a_title == $b_title OR $a_oTitle == $b_title))
                    $lucky = $lucky + 10;
                elseif (($a_title != $b_title OR $a_oTitle != $b_title) OR ($nbr > 1))
                    $lucky = $lucky - 50;
            } else
                $lucky = 0;

            $array[$lucky] = $i;
        }

        krsort($array);
        $choice = array_shift($array);
        $movie = $results[$choice];

        return $movie;
    }

    /**
     * Merge Tmdb movies
     *
     * @access public
     * @param mixed $releases
     * @return void
     */
    public function __mergeMovies($movies)
    {
        $movies = Set::sort($movies, '{n}.id', 'asc');
        $movies_merge = array();

        foreach ($movies as $m) {
            if (empty($m['id']))
                $movies_merge[] = $m;
            else {
                $key = $this->array_search_r($m['id'], $movies_merge);
                if ($key === false) {
                    $movies_merge[] = $m;
                } else {
                    foreach ($m['releases'] as $release)
                        $movies_merge[$key]['releases'][] = $release;
                }
            }
        }

        return $movies_merge;
    }

    /**
     * __readXmlData function.
     *
     * @access public
     * param string $catId
     * @return void
     */
    public function __readXmlData($catId)
    {
        if (empty($catId) OR !is_numeric($catId))
            return false;
        else
            $feedUrl = "http://www.binnews.in/new_rss/cat-" . $catId . ".html";

        $cachePath = 'rss_' . md5($feedUrl);
        Cache::set(array('duration' => $this->config['cacheTime']));
        $data = Cache::read($cachePath);

        if ($data === false) {
            $data = Set::reverse(
                Xml::build(
                    $feedUrl,
                    array(
                        'version' => $this->config['version'],
                        'encoding' => $this->config['encoding']
                    )
                )
            );

            Cache::write($cachePath, serialize($data));
        } else {
            $data = unserialize($data);
        }

        return $data;
    }

    /**
     * __parseXmlItems function.
     *
     * @access public
     * @param mixed $items
     * @return array
     */
    public function __parseXmlItems($data, $cat = null)
    {

        $channel = Set::extract($data, 'rss.channel');
        if (isset($channel['item']))
            unset($channel['item']);

        $items = Set::extract($data, 'rss.channel.item');

        $releases = array();

        foreach ($items as $item) {
            $release = array();

            if (stripos($item['title'], 'A Tous Ceux Qui Prennent Des Films') !== false
                OR stripos($item['title'], 'Un Fichier Avec Mdp ? Lisez Ceci') !== false
                OR stripos($item['title'], '*** MOT DE PASSE ***') !== false
                OR stripos($item['title'], '*** FAKE ***') !== false
                OR stripos($this->__low($item['title']), 'a supprimer') !== false
            ) {
                continue;
            }

            // scrap title & year
            preg_match_all('#([^(]*)(\(.*\)?)?\(([0-9]{4})?\)?#',
                $item['title'],
                $res,
                PREG_SET_ORDER);

            if (empty($res)) continue;

            if (stripos($res[0][1], '3d'))
                $release['b_3d'] = true;

            $title = str_ireplace(array(
                '-', '3d', '(', ')', '[', ']', 'disque 1', 'disque 2', 'version longue', 'BD Européen'
            ), '', $res[0][1]);

            $trans = array('.' => ' ');
            $title = trim(strtr($title, $trans));

            $release['b_title'] = (empty($title)) ? trim((string)$item['title']) : $title; // titre

            $release['b_year'] = !empty($res[0][3]) ? trim(intval($res[0][3])) : ''; // date

            // scrap description
            if (preg_match_all('#Langue :(.*)<br>.*Newsgroup :(.*)<br>.*Nom du fichier :(.*)<br>.*Résolution HD :(.*)<br>.*Taille :(.*)<br>#U',
                $item['description'],
                $res,
                PREG_SET_ORDER)) {
                $release['b_resolution'] = !empty($res[0][4]) ? trim($res[0][4]) : ''; // résolution

                $release['b_size'] = !empty($res[0][5]) ? trim($res[0][5]) : ''; // taille
            } else {
//				preg_match_all('#Langue :(.*)<br>.*Newsgroup :(.*) [-]? .*<br>.*Nom du fichier :(.*)<br>.*Taille :(.*)<br>#U',
                preg_match_all('#Langue :(.*)<br>.*Newsgroup :(.*)<br>.*Nom du fichier :(.*)<br>.*Taille :(.*)<br>#U',
                    $item['description'],
                    $res,
                    PREG_SET_ORDER);

                $release['b_size'] = !empty($res[0][4]) ? trim($res[0][4]) : ''; // taille
            }

            $release['b_lang'] = !empty($res[0][1]) ? trim($res[0][1]) : ''; // langue

            preg_match_all('#(.*)-#U',
                $res[0][2],
                $result,
                PREG_SET_ORDER);

            $group = !empty($result[0][1]) ? $result[0][1] : $res[0][2];

            $release['b_group'] = !empty($group) ? explode(';', trim($group)) : ''; // groupe

            $release['b_name'] = !empty($res[0][3]) ? trim($res[0][3]) : ''; // nom du fichier

            $release['b_comments'] = (string)$item['comments']; // URL du forum

            $release['b_nfo'] = 'http://www.binnews.in/nfo/' . $item['guid']['@'] . '.html'; // URL du NFO

            if ($cat) {
                $release['b_cat'] = $cat;
            }

            $releases[] = $release;
        }

        return $releases;
    }

    /**
     * __filterItems function.
     *
     * @access public
     * @param mixed $items
     * @param mixed $conditions
     * @return void
     */
    public function __filterItems($items = null, $conditions = null)
    {
        if (empty($items) || empty($conditions)) {
            return $items;
        }

        $filteredItems = array();

        foreach ($items as $item) {
            foreach ($conditions as $field => $condition) {
                $itemPassedFilters = $this->__passesXmlCondition($item[$field], $condition);
            }

            if ($itemPassedFilters == true) {
                array_push($filteredItems, $item);
            }
        }
        return $filteredItems;
    }

    /**
     * __passesXmlCondition function.
     *
     * @access public
     * @param mixed $field
     * @param mixed $condition
     * @return void
     */
    public function __passesXmlCondition($field, $condition)
    {
        return preg_match($condition, $field);
    }

    /**
     * __sortItems function.
     *
     * @access public
     * @param mixed &$model
     * @param mixed $items
     * @param mixed $order
     * @return void
     */
    public function __sortItems(&$model, $items, $order)
    {
        if (empty($order[0])) {
            return $items;
        }

        foreach ($order as $orderItem) {
            $sort = array();

            if (is_string($orderItem)) {
                $field = str_replace($model->alias . '.', '', $orderItem);

                foreach ($items as $k => &$v) {
                    $sort[$field][$k] = $v[$field];
                }

                $direction = SORT_ASC;

                array_multisort($sort[$field], $direction, $items);
            } else {
                $sorting = array();
                foreach ($orderItem as $field => $direction) {
                    $sort = array();
                    $values = array();

                    $field = str_replace($model->alias . '.', '', $field);

                    $values = Set::extract($items, '{n}.' . $field);
                    if ($field == 'release_date') {
                        foreach ($values as $i => $value) {
                            $values[$i] = date('Y', strtotime($value));
                        }
                    }
                    // debug($values);

                    switch (strtolower($direction)) {
                        case 'asc':
                            $direction = SORT_ASC;
                            break;
                        case 'desc':
                            $direction = SORT_DESC;
                            break;
                        default:
                            trigger_error('Invalid sorting direction ' . strtolower($direction));
                    }

                    $sorting[] = $values;
                    $sorting[] = $direction;
                }
                // debug($sorting);
                $sorting[] =& $items;

                call_user_func_array('array_multisort', $sorting);
            }

            return $items;
        }
    }

    /**
     * __getPage function.
     *
     * @access public
     * @param mixed $items
     * @param array $queryData
     * @return void
     */
    public function __getPage($items = null, $queryData = array())
    {
        if (empty($queryData['limit'])) {
            return $items;
        }

        $limit = $queryData['limit'];
        $page = $queryData['page'];

        $offset = $limit * ($page - 1);

        return array_slice($items, $offset, $limit);
    }

    /**
     * calculate function.
     *
     * @access public
     * @param mixed &$model
     * @param mixed $func
     * @param array $params
     * @return void
     */
    public function calculate(&$model, $func, $params = array())
    {
        return '__' . $func;
    }

    /**
     * This datasource does not support creating rss feeds
     *
     * @access public
     * @return void
     */
    public function create(Model $model, $fields = null, $values = null)
    {
        return false;
    }

    /**
     * This datasource does not support updating rss feeds
     *
     * @access public
     * @return void
     */
    public function update(Model $model, $fields = null, $values = null, $conditions = null)
    {
        return false;
    }

    /**
     * This datasource does not support deleting rss feeds
     *
     * @access public
     * @return void
     */
    public function delete(Model $model, $conditions = null)
    {
        return false;
    }

    /**
     * in_array recursive
     *
     * @access private
     * @return void
     */
    private function in_array_r($needle, $haystack, $strict = false)
    {
        foreach ($haystack as $item) {
            if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && self::in_array_r($needle, $item, $strict))) {
                return true;
            }
        }
        return false;
    }
}